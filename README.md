# A simple statistics server

This is a somewhat C++17 Linux program which serves UDP requests on the specified port. Requests should contain an event name, and the response will have certain statistics on how much time the event took, on average, in the provided log file. In particular, it will have the minimum response time, the median, and how much time the best 90, 99, 99.9 percent of all entries were processed.

The log may be provided either in a simple text file, from a pipe, and through a tcp socket, which is controlled on startup via commandline parameters. It should contain entries in the following format, of which the second and the last fields are processed.

    [14:10:27] ORDER 518 42 0 0 476

Additionally, the tool prints internal statistics on SIGUSR1/2.