#pragma once
#include <memory>
#include "processor.hh"

static const unsigned UDP_BUFSIZE = 256; // bytes; should be enough for any event name.

class Server
{
    int listening_socket{-1};
    std::unique_ptr<StatisticsProvider> stats;

public:
    Server(unsigned port, std::unique_ptr<StatisticsProvider> stats);
    ~Server();
    void run();
};
