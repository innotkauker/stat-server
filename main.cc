#include <ios>
#include <iostream>
#include <string>
#include <memory>
#include <fstream>
#include <optional>

#include "processor.hh"
#include "server.hh"

void dump_usage()
{
    const char *usage = "\
Usage: stat_server REQUEST_PORT [-d DATA_PORT | -f DATA_FILE] [-l LOG_FILE]\n\
Sets up a UDP server at REQUEST_PORT and serves statistics obtained from either:\n\
    a) TCP socket at DATA_PORT: the log entries shall be fed into the port by one or\n\
        more (consecutive) clients;\n\
    b) file at DATA_FILE;\n\
    c) stdin otherwise.\n\
\n\
Accepts messages of form:\n\
    EVENT\n\
And returns messages of form:\n\
    EVENTNAME min=110 50%=112 90%=122 99%=140 99.9%=145\n\
\n\
Prints statistics on either SIGUSR1 or SIGUSR2 to:\n\
    stdout by default, or\n\
    LOG_FILE if -l is provided\n\
";
    std::cout << usage;
}

static std::optional<char *> get_option(char **begin, char **end, const std::string &option)
{
    auto it = std::find(begin, end, option);
    if (it != end && ++it != end) {
        return *it;
    }
    return std::nullopt;
}

int main(int argc, char *argv[])
{
    std::ios_base::sync_with_stdio(false); // To buffer fd 0 in case we read log from it. Consider moving into the reader.
    if (argc == 1 || argc == 3 || argc == 5 || argc > 6) {
        dump_usage();
        return 1;
    }
    unsigned port = 0;
    try {
        port = std::stoul(argv[1]);
    } catch (const std::logic_error &e) {
        dump_usage();
        return 1;
    }
    std::unique_ptr<StatisticsProvider> provider;
    std::unique_ptr<std::istream> data_source;
    std::unique_ptr<std::ostream> log_handle;
    std::ostream *internal_log = &std::cout;
    if (auto app_log_path = get_option(argv, argv + argc, "-l")) {
        // Dump statistics to a file, not cout.
        log_handle = std::make_unique<std::ofstream>(app_log_path.value());
        internal_log = log_handle.get();
    }
    if (auto log_port_s = get_option(argv, argv + argc, "-d")) {
        // -d and -f are mutually exclusive
        if (get_option(argv, argv + argc, "-f")) {
            dump_usage();
            return 1;
        }
        // Log from a socket.
        unsigned log_port = 0;
        try {
            log_port = std::stoul(log_port_s.value());
        } catch (...) {
            dump_usage();
            return 1;
        }
        provider = std::make_unique<StatisticsProvider>(SocketWrapper(log_port), *internal_log);
    } else if (auto log_path = get_option(argv, argv + argc, "-f")) {
        // Log from a file.
        data_source = std::make_unique<std::ifstream>(log_path.value());
        provider = std::make_unique<StatisticsProvider>(StreamWrapper(*data_source), *internal_log);
    } else {
        // Reading logs from stdin.
        provider = std::make_unique<StatisticsProvider>(StreamWrapper(std::cin), *internal_log);
    }
    Server s(port, std::move(provider));
    s.run();
}
