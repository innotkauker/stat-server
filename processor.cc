#include "processor.hh"
#include <iostream>
#include <algorithm>
#include <iomanip>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <cassert>
#include "util.hh"

EventStatistics TimeDistribution::get_stats()
{
    if (dirty)
        resample();
    return stats;
}

void TimeDistribution::resample()
{
    unsigned targets[] = {
        (unsigned)(total_samples * (1 - 0.5)),
        (unsigned)(total_samples * (1 - 0.9)),
        (unsigned)(total_samples * (1 - 0.99)),
        (unsigned)(total_samples * (1 - 0.999)),
    };
    EventStatistics new_stats;
    unsigned num_samples = 0;
    for (auto it = histogram.rbegin(); it != histogram.rend(); it++) {
        if (*it == 0)
            continue;
        num_samples += *it;
        for (unsigned ti : {0, 1, 2, 3}) {
            if (num_samples >= targets[ti]) {
                targets[ti] = total_samples + 1; // reset the target to avoid matching it later.
                new_stats.levels[ti] = std::distance(histogram.begin(), it.base()); // the time point at which we crossed the threshold.
            }
        }
    }
    auto it = std::find_if(histogram.begin(), histogram.end(), [] (unsigned samples) { return samples != 0; });
    if (it != histogram.end())
        new_stats.inf = std::distance(histogram.begin(), it);
    std::swap(new_stats, stats);
    dirty = false;
}

StatisticsProvider::~StatisticsProvider()
{
    // It's possible to join the file and fifo readers, since they will end at some point.
    // However, the tcp reader will hang waiting for new connections (even if the clients behave as we expect them to).
    // So tell the thread to stop (modifying statistics) and detach. It will be joined on program termination.
    alive->store(false);
    data_fetcher_thread.detach();
}

std::optional<EventStatistics> StatisticsProvider::get_statistics(std::string_view event)
{
    // It should be possible to reduce locking significantly at the cost of less precise statistics.
    // We would only lock on insertion of new nodes into the map, and if all possible events are known
    // ahead, this locking wouldn't be needed either.
    // Or we could use per-distribution locks to somewhat reduce average waiting times without sacrificing preciseness.
    // Also, if the statistics are read from a file in the beginning, we could avoid using locks altogether after it has been read, again without
    // any effect on preciseness.
    std::lock_guard<std::mutex> lock(statistics_access);
    auto it = statistics.find(event);
    if (it == statistics.end()) {
        return std::nullopt;
    } else {
        return it->second.get_stats();
    }
}

struct GroupedTimeDistribution
{
    std::vector<unsigned> bins{std::vector<unsigned>(THRESHOLD_TIME/5, 0)};
    unsigned total{0};

    void add(const TimeDistribution &e)
    {
        for (unsigned i = 0; i != THRESHOLD_TIME; i++) {
            unsigned n = e.histogram[i];
            bins[i/5] += n;
            total += n;
        }
    }
};

void StatisticsProvider::dump_statistics()
{
    std::lock_guard<std::mutex> lock(statistics_access);
    log << "Statistics:" << std::endl;
    GroupedTimeDistribution distribution;
    for (auto &kv : statistics) {
        log << stringify(kv.first, kv.second.get_stats()) << std::endl;
        distribution.add(kv.second);
    }
    log << std::endl;
    log << std::setw(8) << "ExecTime" << "\t" << std::setw(8) << "TransNo" << "\t" << std::setw(8) << "Weight,%" << "\t" << std::setw(8) << "Percent" << std::endl;
    unsigned processed = 0;
    for (unsigned i = 0; i != distribution.bins.size(); i++) {
        if (auto transaction_num = distribution.bins[i]) {
            processed += transaction_num;
            double weight = (double)transaction_num/distribution.total*100;
            double percent = (double)processed/distribution.total*100;
            log << std::setw(8) << i * 5 << "\t" << std::setw(8) << transaction_num
               << "\t" << std::setw(8) << weight << "\t" << std::setw(8) << percent << std::endl;
        }
    }
    log << std::endl;
}

void StatisticsProvider::update_statistics(std::string_view line)
{
    // TODO consider splitting instead of using regex: should be faster.
    // [14:10:27] (ORDER) 518 42 0 0 (476)
    std::cmatch matches;
    if (std::regex_match(line.begin(), line.end(), matches, matcher) && matches.size() == 3) {
        auto event = matches[1].str();
        if (event.size() == 0)
            return;
        unsigned time = 0;
        try {
            time = std::stoul(matches[2].str());
        } catch (const std::logic_error &e) {
            std::clog << "Unexpected log entry: " << line << std::endl;
            return;
        }
        std::lock_guard<std::mutex> lock(statistics_access);
        auto &event_stats = statistics[event];
        event_stats.histogram[std::min(time, THRESHOLD_TIME - 1)]++;
        event_stats.total_samples++;
        event_stats.dirty = true;
    } else {
        std::clog << "Unexpected log entry: " << line << std::endl;
    }
}

StreamWrapper::StreamWrapper(std::istream &stream)
    : stream(stream)
{
}

std::optional<std::string_view> StreamWrapper::next_line()
{
    if (stream.good()) {
        std::getline(stream, buffer);
        return buffer;
    } else {
        return std::nullopt;
    }
}

SocketWrapper::SocketWrapper(unsigned port)
    : listening_port(port)
{
}

void SocketWrapper::initialize_socket()
{
    listening_socket = unwrap(socket(AF_INET, SOCK_STREAM, IPPROTO_TCP), "socket");

    struct sockaddr_in serv_addr;
    memset((char *)&serv_addr, 0, sizeof(serv_addr));

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = INADDR_ANY;
    serv_addr.sin_port = htons(listening_port);

    unwrap(bind(listening_socket, (struct sockaddr *)&serv_addr, sizeof(serv_addr)), "bind");

    unwrap(listen(listening_socket, connection_queue_len), "listen");
}

SocketWrapper::~SocketWrapper()
{
    close(connection_socket);
    close(listening_socket);
}

// Tries to accept a new connection.
void SocketWrapper::obtain_provider()
{
    struct sockaddr_in cli_addr;
    unsigned clilen = sizeof(cli_addr);

    // TODO: likely need to handle errors here.
    connection_socket = unwrap(accept(listening_socket, (struct sockaddr *) &cli_addr, &clilen), "accept");

    // printf("SocketWrapper: got provider from %s port %d\n", inet_ntoa(cli_addr.sin_addr), ntohs(cli_addr.sin_port));
}

// Returts whether we were able to fetch new data from the same provider.
bool SocketWrapper::fetch_data()
{
    if (listening_socket == -1) {
        // This has to be done in the worker thread.
        initialize_socket();
    }
    if (connection_socket == -1) {
        obtain_provider();
    }
    // TODO likely handle errors...
    unsigned data_available = unwrap(read(connection_socket, &net_buffer[0], TCP_BUFSIZE), "read");
    // printf("Read %d bytes from the provider\n", data_available);
    start = net_buffer.begin();
    finish = start + data_available;
    if (!data_available || net_buffer[data_available - 1] == 0) {
        // Either the provider is gone or it has reported "end-of-data"; Need to switch to another provider.
        unwrap(close(connection_socket), "close");
        connection_socket = -1;
        if (data_available)
            finish--; // Drop the trailing EOF.
        if (data_available <= 1)
            return false; // Didn't read anything new here.
    }
    return true;
}

std::optional<std::string_view> SocketWrapper::next_line()
{
    while (start == finish) {
        // While in case there are some "empty" providers.
        fetch_data();
    }
    auto it = std::find(start, finish, '\n');
    if (it == finish) {
        // We have read till the end of the buffer waiting for a newline.
        // Save the start of the line in a temporary buffer and fetch a new buffer.
        line_buffer = std::string(start, finish - start);
        if (fetch_data()) {
            it = std::find(start, finish, '\n');
            assert(it != finish); // otherwise we need a bit more complicated logic...
            line_buffer.append(start, it);
            start = it + 1;
        } else {
            start = finish;
        }
        return line_buffer;
    } else {
        auto line_beginning = start;
        start = it + 1; // skip the newline character
        return std::string_view(line_beginning, std::distance(line_beginning, it));
    }
}

int unwrap(int result, const char *method)
{
    if (result == -1) {
        int err = errno;
        auto err_descr = strerror(err);
        std::stringstream ss;
        ss << method << "() failed: " << err_descr;
        throw ss.str();
    }
    return result;
}

