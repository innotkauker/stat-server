FILES = main processor server
HEADERS = processor.hh server.hh util.hh
SOURCES = $(patsubst %,%.cc,$(FILES))
OBJS = $(patsubst %,%.o,$(FILES))
CXX                   = g++
CXXFLAGS              = -std=c++17 -g -Wall -I.

.PHONY: all run clean default

default: all

$(OBJS): %.o : %.cc $(HEADERS)
	@echo '    CXX' $@;
	@$(CXX) $(CXXFLAGS) -c $(patsubst %.o,%.cc,$@) -o $@

stat-server: $(OBJS)
	@echo "    $@";
	@$(CXX) -o $@ $(OBJS) -pthread

all: stat-server

clean:
	rm -f *.o stat-server

