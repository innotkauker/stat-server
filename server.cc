#include "server.hh"
#include <sys/socket.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <unistd.h>
#include <cerrno>
#include <cstdio>
#include <cstring>
#include <sstream>
#include <iostream>

#include "util.hh"

Server::Server(unsigned port, std::unique_ptr<StatisticsProvider> s)
    : stats(std::move(s))
{
    listening_socket = unwrap(socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP), "socket");
    struct sockaddr_in si_me;
    memset((char *)&si_me, 0, sizeof(si_me));
    si_me.sin_family = AF_INET;
    si_me.sin_port = htons(port);
    si_me.sin_addr.s_addr = htonl(INADDR_ANY);
    unwrap(bind(listening_socket, (struct sockaddr *)&si_me, sizeof(si_me)), "bind");
}

Server::~Server()
{
    close(listening_socket);
}

std::string stringify(std::string_view event, std::optional<EventStatistics> stats)
{
    // auto fmt = "%s min=%d 50%=%d 90%=%d 99%=%d 99.9%=%d";
    EventStatistics result;
    if (stats)
        result = stats.value();
    std::stringstream ss;
    ss << event << " min=" << result.inf << " 50%=" << result.levels[0]
       << " 90%=" << result.levels[1] << " 99%=" << result.levels[2] << " 99.9%=" << result.levels[3];
    return ss.str();
}

void Server::run()
{
    // Can add a stopping condition here in case we would like to exit "gracefully"
    while (true)
    {
        // std::clog << "Waiting for requests" << std::endl;

        struct sockaddr_in si_other;
        unsigned sockaddr_in_len = sizeof(si_other);
        char buf[UDP_BUFSIZE];
        int recv_len = unwrap(recvfrom(listening_socket, buf, UDP_BUFSIZE, 0, (struct sockaddr *) &si_other, &sockaddr_in_len), "recvfrom");

        // printf("Received packet from %s:%d\n", inet_ntoa(si_other.sin_addr), ntohs(si_other.sin_port));
        // printf("Data: %s\n", buf);

        unsigned msg_len = recv_len;
        if (buf[recv_len-1] == '\n')
            msg_len--;
        const auto event = std::string_view(&buf[0], msg_len);
        const auto reply = stringify(event, stats->get_statistics(event));
        // printf("Reply: %s\n", reply.c_str());

        unwrap(sendto(listening_socket, reply.c_str(), reply.size(), 0, (struct sockaddr*) &si_other, sockaddr_in_len), "sendto");
    }
}

