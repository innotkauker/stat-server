#pragma once
#include <string>
#include <string_view>
#include <optional>

// An unwrapper for C lib functions and syscalls that we don't expect to fail:
// if result is -1, we throw the error descr, otherwise we return the result unmodified.
int unwrap(int result, const char *method);

std::string stringify(std::string_view event, std::optional<EventStatistics> stats);
