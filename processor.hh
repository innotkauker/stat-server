#pragma once
// This file contains code for processing input logs.
#include <optional>
#include <string>
#include <string_view>
#include <istream>
#include <map>
#include <mutex>
#include <thread>
#include <vector>
#include <atomic>
#include <regex>
#include <signal.h>
#include <pthread.h>

static const unsigned TCP_BUFSIZE = 256; // bytes
static const unsigned THRESHOLD_TIME = 1000; // microseconds

struct EventStatistics
{
    unsigned inf{0}; // minimal delay
    // 50, 90, 99, 99.9%
    std::array<unsigned, 4> levels{0, 0, 0, 0};
};

struct TimeDistribution
{
    EventStatistics stats;
    std::vector<unsigned> histogram{std::vector<unsigned>(THRESHOLD_TIME, 0)}; // Requests that took >1ms are truncated to 1ms.
    unsigned total_samples{0};
    bool dirty{true}; // TODO: might also introduce a timeout in case requests are happening too often.

    void resample();
public:
    TimeDistribution() {}
    EventStatistics get_stats();
};

class StatisticsProvider
{
    // TODO consider using boost's hashmap here - it would support lookup by string_views. Stl hashmap doesn't.
    std::map<std::string, TimeDistribution, std::less<>> statistics;
    std::mutex statistics_access; // statistics might be modified by one thread and read by another - so guard it.
    std::thread data_fetcher_thread; // this thread fetches log entries from the DataSource and updates the map.
    std::thread signal_handler_thread; // this thread prints statistics on SIGUSR.
    std::shared_ptr<std::atomic<bool>> alive; // to prevent the worker thread from modifying this object after destruction.
    std::regex matcher; // for parseable log lines.
    std::ostream &log; // dump statistics here on signal.

    void update_statistics(std::string_view line);
    void dump_statistics();
public:
    // TODO consider providing a concept here.
    template<typename DataSource>
    StatisticsProvider(DataSource data, std::ostream &log);

    ~StatisticsProvider();
    std::optional<EventStatistics> get_statistics(std::string_view event);
};

// Provides transaction information when log is provided from file or from pipe.
class StreamWrapper
{
    std::istream &stream;
    std::string buffer; // next_line returns the contents of this buffer.
public:
    StreamWrapper(std::istream &stream);
    // Will return a nullopt when there is no more data in the file.
    std::optional<std::string_view> next_line();

    ~StreamWrapper() = default;
    StreamWrapper(const StreamWrapper &) = delete;
    StreamWrapper &operator=(const StreamWrapper &) = delete;
    StreamWrapper(StreamWrapper &&) = default;
    StreamWrapper &operator=(StreamWrapper &&) = default;
};

// Provides transaction info from a TCP socket.
class SocketWrapper
{
    unsigned listening_port{8000};
    int listening_socket{-1};
    unsigned connection_queue_len{2};
    int connection_socket{-1}; // We support only one simultaneous data supplier.
    typedef std::array<char, TCP_BUFSIZE> DataStorage;
    DataStorage net_buffer;
    DataStorage::iterator start{net_buffer.end()}; // these characterize data available in net_buffer
    DataStorage::iterator finish{net_buffer.end()};
    std::string line_buffer; // next_line returns the contents of either this or net_buffer.

    void initialize_socket();
    void obtain_provider();
    bool fetch_data();
public:
    SocketWrapper(unsigned port);
    // This is a blocking call; it will either return a line or hang waiting for it.
    // Result is optional to make it interface-compatible with the other data provider.
    std::optional<std::string_view> next_line();

    ~SocketWrapper();
    SocketWrapper(const SocketWrapper &) = delete;
    SocketWrapper &operator=(const SocketWrapper &) = delete;
    SocketWrapper(SocketWrapper &&) = default;
    SocketWrapper &operator=(SocketWrapper &&) = default;
};

template<typename DataSource>
StatisticsProvider::StatisticsProvider(DataSource data, std::ostream &log)
    : log(log)
{
    matcher = std::regex("[^\t]*\t*([^\t]*)\t*[^\t]*\t*[^\t]*\t*[^\t]*\t*[^\t]*\t*([^\t]*)");
    // This one controls shutdown of threads - in case this is ever needed.
    alive = std::make_shared<std::atomic<bool>>(true);
    // Block USR signals so that the handler thread can pick them up.
    sigset_t signal_set;
    sigemptyset(&signal_set);
    sigaddset(&signal_set, SIGUSR1);
    sigaddset(&signal_set, SIGUSR2);
    pthread_sigmask(SIG_BLOCK, &signal_set, NULL);
    // Start the log fetcher thread.
    auto fetcher = std::thread([this, data = std::move(data), alive = alive] () mutable -> void {
        while (auto line = data.next_line()) {
            if (alive->load() == false)
                break;
            update_statistics(line.value());
        }
    });
    data_fetcher_thread = std::move(fetcher);
    // Start the signal handler thread.
    auto sig_handler = std::thread([this, alive = alive, signal_set] () {
        int sig = 0;
        while (true) {
            if (!sigwait(&signal_set, &sig)) {
                if (alive->load() == false)
                    break;
                dump_statistics();
            }
        }
    });
    signal_handler_thread = std::move(sig_handler);
}

